module.exports = function (grunt) {
   require('time-grunt')(grunt);
   require('jit-grunt')(grunt, {
       useminPrepare: 'grunt-usemin'
   });
   
   
   
    grunt.initConfig ({
         sass :{
             dist:{
                 file: [{
                     expand: true,
                     cwd:'css',
                     src: ['*.scss'],
                     dest:'../css',
                     ext:'.css'
                 }]
             }
         },

         watch:{
             file:['css/*.scss'],
             task:['css']
         },
         
         browserSync:{
                dev:{
                    bsFiles:{ //browser file
                        src:[
                            'css/*.css',
                            '*.html',
                            'js/*.js'
                        ]

                    },
                    options:{
                        watchTask: true,
                        server: {
                            baseDir: './' //Directorio de base de servidor
                        }
                    }
                }
             
         },

         imagemin:{
             dynamic:{
                 file:[{
                     expand:true,
                     cwd:'./',
                     src:'imagen/*.{png,gif,jpg,jpeg}',
                     dest: 'dist/'
                 }]
             }
         },

         copy:{
             html:{
                 file:[{
                     expand: true,
                     dot: true,
                     cwd:'./',
                     src: ['*.html'],
                     dest: 'dist' 
                 }]
             },
         },

         clean:{
             build: {
                 src: ['dist/']
             }
         },

         cssmin:{
             dist:{}
         },

         uglify:{
             dist: {}
         },

        filerev: {

             options: {
                 encoding: 'uttf8',
                 algorithm: 'md5',
                 length:20
             },
             release: {
                 //filerev:release hashes(md5) all assets (imagen, js and css)
                 // in dist directory
                  files: [{
                      src:[
                          'dist/js/*.js',
                          'dist/css/*.css',
                      ]
                  }]
             }
        },

         concat:{
             options:{
                 separator: ';'
             },
             dist:{}
         },
         
         useminPrepare:{
             foo: {
                 dest: 'dist',
                 src: ['index.html', 'habitaciones.html', 'eventos.html', 'contacto.html', 'precio.html']
                },

             options: {
                 flow: {
                     steps: {
                         css: ['cssmin'],
                         js: ['uglify']

                     },
                     post: {
                         css:[{
                             name: 'cssmin',
                             createConfig: function(context, block) {
                                 var generated = context.options.generated;
                                 generated.options = {
                                     keepSpecialComments: 0,
                                     rebase: false
                                 }
                             }
                         }]
                     }
                 }
             }   
         },

         usemin: {
             html: ['dist/index.html', 'dist/habitaciones.html', 'dist/eventos.html', 'dist/contacto.html', 'dist/precio.html' ],
             options: {
                 assetsDir: ['dist', 'dist/css', 'dist/js']
             }
         }
          

    });
    
    grunt.registerTask('css',['sass']);
    grunt.registerTask('default',['browserSync','watch'])
    grunt.registerTask('img:compress', ['imagemin']);
    grunt.registerTask('build', [
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'
         ] )
};
 